/***** MAC0110 - EP3 *****/
  Nome: Rodrigo Volpe Battistin
  NUSP: 11795464

/***** Parte 1 - Entendendo o código *****/

    1) Qual a probabilidade de um elemento da matriz ser grama (ou terreno)? Você pode expressar essa possibilidade utilizando um valor numérico ou uma fórmula.

        A probabilidade de um elemento ser do tipo terreno (especial ou não) é igual a: 1.0 - (PROBABILIDADE_LOBO + PROBABILIDADE_COELHO + PROBABILIDADE_COMIDA); ou seja, com os valores dados a probabilidade seria (0.65). 

    2) Analise o código e diga: qual a diferença entre o terreno e o terreno especial?

        Analisando os códigos da parte 1, não há uma diferença prática entre o terreno e o terreno especial, apenas que o especial é mais raro de aparecer. Isso pode ser visto na função "processa_terreno!(ilha, energia)", especificamente na linha 134, pois o OU (||) demonstra que não faz diferença ser o terreno especial ou normal.

    3) Dados os valores iniciais das constantes, qual a energia necessária para um lobo se reproduzir? E um coelho?

        A energia necessária para um lobo se reproduzir é 20 (2 * 10), enquanto que para um coelho é 12 (2 * 6).

/***** Parte 2 - Completando as funções *****/

    4) Ao contrário das funções ocupa_vizinho! e reproduz!, a função morre! não recebe a matriz de energia como parâmetro. Por quê não é necessário alterar a energia com a morte de um animal?

        Porque a única situação em que um animal irá morrer (não sendo devorado) é quando a sua energia chega em 0, que é exatamente a energia dos elementos do tipo terreno, portanto não é preciso alterar esse parâmetro. A energia daquele animal se igualar à energia de um terreno é um pré-requisito para ativar a função "morre!", logo a matriz de energia nunca precisará ser alterada.

/***** Parte 3 - Teste e simulação *****/

    5) Usando os valores iniciais das constantes e após uma quantidade considerável de iterações, mais de 30, por exemplo, o que acontece na ilha?

        A ilha eventualmente se encontra sem nenhum coelho, pois a maioria foi devorada por lobos (enquanto outros morreram de fome), algo que pode ser notado por volta da iteração 20. Após a extinção dos coelhos, a população de lobos começa a diminuir, enquanto que a população de cenouras começa a aumentar, eventos esses que condizem com a relação na cadeia alimentar.

    6) Qual combinação de constantes leva a ilha a ser dominada por coelhos? Utilize uma ilha de tamanho 20 ou mais, e simule utilizando uma quantidade considerável de iterações.

        A combinação mais simples seria: PROBABILIDADE_LOBO = 0.0 e REGENERAÇÃO_TERRENO = 0.5, por exemplo, mantendo o resto das constantes com seus valores originais.

    7) Qual combinação de constantes leva a ilha não ter nenhum animal? Utilize uma ilha de tamanho 20 ou mais, e simule utilizando uma quantidade considerável de iterações.

        Basta diminuir a REGENERAÇÃO_TERRENO para algo como 0.001 e manter a simulação por um número significativo de iterações, como 50.

/***** Parte 3 - Usando DataFrames e plotando gráficos *****/

    8) Qual a diferença entre a função simula - da parte 3 - e a função simula2?

        A função simula2 gera uma matriz dos dados sobre a simulação da ilha (DataFrame), com o número determinado de iterações e encadeando cada um dos estados produzidos na matriz que vai sendo construída, devolvendo esse DataFrame ao final da execução. Ela também tem a possibilidade de imprimir na tela do terminal exatamente o que a função simula (original) mostraria, caso o argumento "imprime" for true.

    9) A função gera_graficos possui sintaxes diferentes das vistas em aula e nos outros exercícios. Apesar disso, é possível entender o que a função faz, sem rodá-la e sem conhecer detalhes sobre os pacotes de gráficos. Sem rodar a função, responda: quantos gráficos a função plota? Qual o conteúdo de cada gráfico?

        A função plota dois gráficos, um mostrando a relação entre o número de lobos e coelhos a cada iteração da simulação ("passo"), enquanto o outro mostra a relação entre o número de cenouras (comida) e a energia total presente na ilha, também a cada iteração. Possivelmente a relação mostrada é a diferença entre os dois valores, uma suposição baseada no macro "@df" utilizado, que talvez signifique "difference".

    10) Usando os valores iniciais das constantes e após uma quantidade considerável de iterações, mais de 30, por exemplo, o que acontece nos gráficos mostrados? Houve alguma semelhança ou diferença no que você respondeu na parte 3?

        Os gráficos mostram que a população de coelhos é extinta por volta da iteração 20, a seguir a população de lobos começa a decair e, após certo tempo, a população de cenouras começa a lentamente aumentar. Houve muitas semelhanças com relação à resposta dada na parte 3, sendo que alguns detalhes ficam mais perceptíveis quando analisamos os gráficos gerados: a população de lobos tem um incremento ao mesmo tempo em que a de coelhos decresce, mas eventualmente todos os coelhos são devorados e os lobos começam a morrer de fome; além disso, a COMIDA passa a ter um aumento controlado a partir do passo 40, aproximadamente.

    11) Usando a combinação de constantes da questão 6 - que leva a ilha a ser dominada por coelhos - o que acontece nos gráficos mostrados? Houve alguma semelhança ou diferença no que você respondeu naquela questão?

        Os gráficos mostram que a população de coelhos tem um crescimento acentuado até a iteração 40, aproximadamente, e entre o passo 40 e 50 ela se estabiliza; além disso, fica evidente que a quantidade de cenouras permanece baixa durante toda a simulação, enquanto que a energia total tem um pico pouco antes da população de coelhos estar em seu auge, e depois disso esses dois gráficos apresentam comportamentos semelhantes. A resposta dada na questão 6 é comprovada com os gráficos plotados, então há somente semelhanças.

    12) Usando a combinação de constantes da questão 7 - que leva a ilha à extinção de todos os animais - o que acontece nos gráficos mostrados? Houve alguma semelhança ou diferença no que você respondeu naquela questão?

        Os gráficos demonstram que, por volta da iteração 35, todos os animais já estão extintos, sendo que os coelhos são os primeiros a desaparecer (entre o passo 10 e 20) e os lobos sobrevivem por mais algum tempo; além disso, analisando o segundo gráfico, a energia total da ilha decresce vertiginosamente no período em que os animais estão morrendo, mas após a extinção em massa ela se estabiliza. Não houve diferença para o que foi respondido na questão 7, pois em ambos os casos foi visto que a ilha eventualmente se encontra sem nenhum animal.
